# Design Patterns
This repo contains implementations of the original Gang of Four Design Patterns in the Java language.
As time goes on more languages may be added.

The patterns are :-
* Creational: Create objects for you.
   * Abstract Factory
   * Builder
   * Factory method
   * Prototype
   * Singleton
* Structural
   * Adapter
   * Bridge
   * Composite
   * Decorator
   * Facade
   * Flyweight
   * Proxy
* Behavioural: Communication between objects
   * Chain of responsibility
   * Command
   * Interpreter
   * Iterator
   * Mediator
   * Memento
   * Observer
   * State
   * Strategy
   * Templayte
   * Visitor


## References
* http://www.blackwasp.co.uk/gofpatterns.aspx
* https://en.wikipedia.org/wiki/Design_Patterns#Patterns_by_Type

